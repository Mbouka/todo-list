FROM openjdk:18
WORKDIR /app
COPY ./target/spring-0.0.1-SNAPSHOT.jar /spring-0.0.1-SNAPSHOT.jar
EXPOSE 8076
CMD ["java", "-jar", "spring-0.0.1-SNAPSHOT.jar"]