package org.isj.ing.annuarium.webapp.Annuarium.service;

import org.isj.ing.annuarium.webapp.Annuarium.model.entities.Todolist;
import org.isj.ing.annuarium.webapp.Annuarium.repository.TodoListRespository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes =ITodoList.class )
public class ITodoListTest {
    @Mock
    private TodoListRespository todoListRespository;
    @MockBean
    private TodoListService listService;

    private Todolist todolist;
    private ITodoList iTodoList;



    @DisplayName("Test for saving a task method")
    @Test
    public void testsaveTodo() throws Exception{
        Todolist todo1 = new Todolist();
        todo1.setId(1L);
        todo1.setStatus("now");
        todo1.setTitle("Saving test succeeded");
       given(todoListRespository.findTodoListByTitle(todo1.getTitle())).willReturn(Optional.empty());
       given(todoListRespository.save(todo1)).willReturn(todo1);
       Todolist sav= todoListRespository.save(todo1);
        System.out.println(sav);
    }

    @Test
    void updateTodo() {
        /*Todolist todo2 = new Todolist();
        todo2.setId(2L);
        todo2.setStatus("urgent");
        todo2.setTitle("item1 in list");
        Todolist todo4 = new Todolist();
        todo4.setId(5L);
        todo4.setStatus("now");
        todo4.setTitle("item3 in list");
        Todolist todo3 = new Todolist();
        todo3.setId(3L);
        todo3.setStatus("now");
        todo3.setTitle(" in list");
        given(todoListRespository.findById(3L)).willReturn(Optional.of(todolist));
        //todo3.setTitle("updating");
        //given(todoListRespository.save(todo3)).willReturn(todo3);
        Todolist update= listService.updateTodo(todo3,3L);
        //Todolist upload = listService.updateTodo(todolist,1L);
        System.out.println(todo3);*/
    }

    @Test
   public void listTodo() throws Exception {
        Todolist todo2 = new Todolist();
        todo2.setId(2L);
        todo2.setStatus("urgent");
        todo2.setTitle("item1 in list");
        /*********/
        Todolist todo4 = new Todolist();
        todo4.setId(5L);
        todo4.setStatus("now");
        todo4.setTitle("item3 in list");
        /*********/
        Todolist todo3 = new Todolist();
        todo3.setId(3L);
        todo3.setStatus("now");
        todo3.setTitle("item1 in list");
        /*********/
        given(todoListRespository.findAll()).willReturn(Collections.emptyList());
        List<Todolist> todolistList = listService.listTodo();
        todolistList.add(todo2);
        todolistList.add(todo4);
        todolistList.add(todo3);

        System.out.println(todolistList);

    }

    @Test
    void deleteTodo() {
      todoListRespository.deleteById(3L);

    }

    @Test
    void findTodo() {
     /*   given(todoListRespository.findById(1L)).willReturn(Optional.of(todolist));
        Todolist Find = listService.findTodo(todolist.getId()).get();
        System.out.println(Find);*/
    }

    @Test
    void searchTodoListByTitle() {
    }

    @Test
    void countTask() {
        long c = listService.countTask();
        System.out.println(c);
    }
    private void getTodolist(){
        Todolist todo = new Todolist();
        todo.setId(1L);
        todo.setStatus("now");
        todo.setTitle("go");
        Todolist saveTodolist = todoListRespository.save(todo);
    }
}